import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmpService {
  loginStatus: boolean;
  
//Dependency Injection for HttpClient
  constructor(private http: HttpClient) {
    this.loginStatus = false;
  }
  
  //Fetching all countries from rest countries api
  getAllCountries(): any {
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  isUserLoggedIn() {
    this.loginStatus = true;
  }

  isUserLoggedOut() {
    this.loginStatus = false;
  }

  getLoginStatus(): boolean {
    return this.loginStatus;
  }
   }

